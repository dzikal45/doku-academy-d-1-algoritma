import java.util.Scanner;

public class LampudanTombol {
    public static void main(String[] args) {
        boolean lampu = false;
        Scanner sc= new Scanner(System.in);
        int n = sc.nextInt();

        for (int i= 1;i<=n;i++){
            if(n %i == 0){
                lampu = !lampu;
            }
        }

        if (lampu){
            System.out.println("lampu nyala");
        }else{
            System.out.println("lampu mati");
        }

    }
}
